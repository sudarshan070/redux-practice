import React, { useEffect } from "react";
import { connect } from "react-redux";
import { getMovieList } from "../action";

// class Movies extends React.Component {
//   componentDidMount() {
//     this.props.dispatch(getMovieList());
//   }

//   render() {
//     return (
//       <div>
//         <p>Movies</p>
//       </div>
//     );
//   }
// }

function Movies(props) {
  useEffect(() => {
    props.dispatch(getMovieList());
  }, []);
  return (
    <div>
      {console.log(props.movies)}
      {props.movies.results
        ? props.movies.results.map((movie) => {
            return (
              <div key={movie.id}>
                <p>{movie.title}</p>
              </div>
            );
          })
        : ""}
      <p>hello</p>
    </div>
  );
}

function mapToState({ getMovieList }) {
  return { ...getMovieList };
}

export default connect(mapToState)(Movies);
