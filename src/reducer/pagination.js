const paginationRouter = (state = 1, action) => {
    switch (action.type) {
        case "NEXTPAGE":
            return state + 1
        case "PREVIOUSPAGE":
            return state - 1
        default:
            return state
    }
}

export default paginationRouter