
const initialState = {
    movies: [],
    page: 1
}

const getMoviesReducer = (state = initialState, action) => {

    switch (action.type) {
        case "GET_MOVIES":
            return {
                ...state,
                movies: action.data,
                page: action.data
            }
        default:
            return {
                ...state
            }
    }

}

export default getMoviesReducer