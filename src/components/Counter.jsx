import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { increment, decrement } from "../action";

export default function Counter() {
  const counterReducer = useSelector((state) => state.counterReducer);
  const dispatch = useDispatch();
  return (
    <div>
      <h1>Counter {counterReducer} </h1>
      <button onClick={() => dispatch(increment())}>+</button>
      <button onClick={() => dispatch(decrement())}>-</button>
    </div>
  );
}
