import React from 'react';
import Counter from './components/Counter';
import Movies from './components/Movies';
import Test from './components/Test';


function App() {
  return (
    <div >
      <Counter />
      <Movies />
      {/* <Test /> */}
    </div>
  );
}
export default App;
