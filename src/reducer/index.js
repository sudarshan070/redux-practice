import { combineReducers } from '@reduxjs/toolkit'
import counterReducer from './counter'
import getMovieList from './movies'
import isLogged from './isLogged'

const rootReducer = combineReducers({
    counterReducer,
    isLogged,
    getMovieList
})

export default rootReducer