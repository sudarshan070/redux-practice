import axios from "axios"

export const increment = () => {
    return {
        type: "INCREMENT"
    }
}

export const decrement = () => {
    return {
        type: "DECREMENT"
    }
}

export const nextPage = () => {
    return {
        type: "NEXTPAGE"
    }
}

export const previousPage = () => {
    return {
        type: "PREVIOUSPAGE"
    }
}


export const getMovieList = () => {
    const page = 17
    return ((dispatch) => {
        axios.get(`https://api.jikan.moe/v3/search/anime?q=<query>&limit=24&page=${page}`).then(res => {
            console.log(res.data, 'data is here')
            dispatch({
                type: "GET_MOVIES",
                data: res.data
            })
        }).catch(err => console.log(err))
    })
}