import axios from "axios";
import React, { useEffect } from "react";

export default function Test() {
  useEffect(() => {
    axios
      .get(`https://api.jikan.moe/v3/search/anime?q=<query>&limit=24&page=1`)
      .then((res) => console.log(res.data, "res is here test"));
  }, []);
  return <div></div>;
}
